Application.$controller("DetailsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {

    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        if ($scope.pageParams.BookingId == 0 || $scope.Variables.svBookingID.dataSet.dataValue == 0)
            $scope.Variables.goToPage_Main.navigate();

    };

    $scope.svGetPassengersbyBookingIDonSuccess = function(variable, data) {
        if (data.content.length >= $scope.Variables.lvBookingDetails.dataSet.data[0].totalPassengers) {
            $scope.Widgets.addPassengerButton.disabled = true;
            $scope.Widgets.proceedToPaymentButton.disabled = false;
        }
    };



}]);

Application.$controller("grid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


    }
]);
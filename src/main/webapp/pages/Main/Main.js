Application.$controller("MainPageController", ["$scope", "$timeout", function($scope, $timeout) {
    "use strict";
    $scope.to_calculated_fare = 0;
    $scope.fro_calculated_fare = 0;
    $scope.dollar = 66;
    $scope.onPageVariablesReady = function() {

    };

    $scope.onPageReady = function() {
        $scope.toClass = "Economy";
        $scope.froClass = "Economy";

        $(".datepickerinput-to").datepicker({
            numberOfMonths: 2,
            dateFormat: "yy-mm-dd",
            minDate: +0
        });

        $(".datepickerinput-fro").datepicker({
            numberOfMonths: 2,
            dateFormat: "yy-mm-dd",
            minDate: +0
        });
    };

    function checkSameAirPorts() {
        if ($scope.Widgets.fromAirport.datavalue == $scope.Widgets.toAirport.datavalue) {
            $scope.Variables.SameAirports.notify();
        }
    }
    $scope.fromAirportChange = function($event, $isolateScope) {
        checkSameAirPorts();
    };

    $scope.toAirportChange = function($event, $isolateScope) {
        checkSameAirPorts();
    };

    $scope.radioset3Change = function($event, $isolateScope) {
        $event.stopPropagation();
        var x = $isolateScope.datavalue;
        $scope.selectedRadio = Number(x);
        $scope.Variables.selected_Radio.dataSet.dataValue = $scope.selectedRadio;
        if ($scope.Widgets.to_flight_livelist.selecteditem.economyClassFare == $scope.selectedRadio || ($scope.Widgets.to_flight_livelist.selecteditem.economyClassFare) / $scope.dollar == $scope.selectedRadio) {
            $scope.toClass = "Economy";
        }
        if ($scope.Widgets.to_flight_livelist.selecteditem.businessClassFare == $scope.selectedRadio || ($scope.Widgets.to_flight_livelist.selecteditem.businessClassFare) / $scope.dollar == $scope.selectedRadio) {
            $scope.toClass = "Business";
        }
        if ($scope.Widgets.to_flight_livelist.selecteditem.firstClassFare == $scope.selectedRadio || ($scope.Widgets.to_flight_livelist.selecteditem.firstClassFare) / $scope.dollar == $scope.selectedRadio) {
            $scope.toClass = "First Class";
        }
        $scope.calculateAirFare();
    };


    $scope.lvCreateBookingonSuccess = function(variable, data) {

        // [Vibhu]: Direct Binding
        $scope.Variables.svBookingID.dataSet.dataValue = data.id;
    };



    $scope.toJourneyDtChange = function($event, $isolateScope) {
        var toDate = $(".datepickerinput-to").datepicker("getDate");
        $(".datepickerinput-fro").datepicker("option", "minDate", toDate);
    };


    $scope.calculateAirFare = function() {
        // get the class the user has selected
        var selected_class = $scope.Widgets.selected_class.datavalue;

        // get number of passengers
        var number_of_adults = $scope.Widgets.adultNo.caption;
        var number_of_children = $scope.Widgets.childNo.caption;
        var number_of_infants = $scope.Widgets.infantNo.caption;
        var number_of_passengers = number_of_adults + number_of_children + number_of_infants;

        // get fares;
        var to_economy_class_fare = $scope.Widgets.to_flight_livelist.selecteditem.economyClassFare;
        var to_business_class_fare = $scope.Widgets.to_flight_livelist.selecteditem.businessClassFare;
        var to_first_class_fare = $scope.Widgets.to_flight_livelist.selecteditem.firstClassFare;
        var fro_economy_class_fare = $scope.Widgets.return_flight_livelist.selecteditem.economyClassFare;
        var fro_business_class_fare = $scope.Widgets.return_flight_livelist.selecteditem.businessClassFare;
        var fro_first_class_fare = $scope.Widgets.return_flight_livelist.selecteditem.firstClassFare;

        // calculate fares based on class
        $scope.to_calculated_fare = 0;
        $scope.to_calculated_fare = $scope.selectedRadio * number_of_passengers;
        $scope.fro_calculated_fare = 0;
        $scope.fro_calculated_fare = $scope.selectedFroRadio * number_of_passengers;

        //set the values in the UI
        if (isNaN($scope.to_calculated_fare)) {
            $scope.to_calculated_fare = 0;
        }

        if (isNaN($scope.fro_calculated_fare)) {
            $scope.fro_calculated_fare = 0;
        }

        $scope.Variables.towardsFare.dataSet.dataValue = $scope.to_calculated_fare;
        $scope.Variables.returnFare.dataSet.dataValue = $scope.fro_calculated_fare;

        if ($scope.Widgets.selected_trip.datavalue == 'Round Trip') {
            if ($scope.to_calculated_fare > 0 && $scope.fro_calculated_fare > 0) {
                $scope.Widgets.buttonBook.disabled = false;
            } else {
                $scope.Widgets.buttonBook.disabled = true;
            }
        } else {
            if ($scope.to_calculated_fare > 0) {
                $scope.Widgets.buttonBook.disabled = false;
            } else {
                $scope.Widgets.buttonBook.disabled = true;
            }
        }

    }

    $scope.radioset4Change = function($event, $isolateScope) {
        var x = $isolateScope.datavalue;
        $scope.selectedFroRadio = Number(x);
        $scope.Variables.selected_fro_Radio.dataSet.dataValue = $scope.selectedRadio;
        if ($scope.Widgets.return_flight_livelist.selecteditem.economyClassFare == $scope.selectedFroRadio || ($scope.Widgets.return_flight_livelist.selecteditem.economyClassFare) / $scope.dollar == $scope.selectedFroRadio) {
            $scope.froClass = "Economy";
        }
        if ($scope.Widgets.return_flight_livelist.selecteditem.businessClassFare == $scope.selectedFroRadio || ($scope.Widgets.return_flight_livelist.selecteditem.businessClassFare) / $scope.dollar == $scope.selectedFroRadio) {
            $scope.froClass = "Business";
        }
        if ($scope.Widgets.return_flight_livelist.selecteditem.firstClassFare == $scope.selectedFroRadio || ($scope.Widgets.return_flight_livelist.selecteditem.firstClassFare) / $scope.dollar == $scope.selectedFroRadio) {
            $scope.froClass = "First Class";
        }
        $scope.calculateAirFare();
    };



    //For Removing Dates Before To-Journey Dates
    $scope.to_dates_livelistClick = function() {
        var fromDate = $scope.Widgets.to_dates_livelist.selecteditem.DEPARTURE_DATE;
        var $to = $('[name="return_dates_livelist"] > ul > li.app-list-item');

        $to.each(function() {
            var $li = $(this);
            var toDate = $li.scope().item.DEPARTURE_DATE;

            if (moment(fromDate).valueOf() > moment(toDate).valueOf()) {
                $li.addClass('ng-hide');
            } else {
                $li.removeClass('ng-hide');
            }
        });

        $scope.$evalAsync(function() {
            $('[name="return_dates_livelist"] > ul > li.app-list-item:not(".ng-hide")').first().click();
        });

    }

    $scope.get_flights_between_datesonSuccess = function(variable, data) {
        if (data.totalElements == 0) {
            $scope.Widgets.containerNoFlights.show = true;
            $scope.Widgets.containerFlights.show = false;
        } else {
            $scope.Widgets.containerNoFlights.show = false;
            $scope.Widgets.containerFlights.show = true;
        }
    };

    $scope.buttonBookClick = function($event, $isolateScope) {
        if ($scope.Widgets.selected_trip.datavalue == 'Round Trip') {
            if ($scope.Variables.towardsFare.dataSet.dataValue == 0 || $scope.Variables.returnFare.dataSet.dataValue == 0) {
                $scope.Variables.bothSelectWarning.notify();
            } else {
                $scope.Variables.lvCreateBooking.insertRecord();
            }

        } else {
            if ($scope.Variables.towardsFare.dataSet.dataValue == 0) {
                $scope.Variables.toSelectWarning.notify();
            } else {
                $scope.Variables.lvCreateBooking.insertRecord();
            }
        }
    };

    $scope.selectCurrencyChange = function($event, $isolateScope) {
        if ($isolateScope.datavalue == "INR") {
            $scope.Variables.towardsFare.dataSet.dataValue = ($scope.Variables.towardsFare.dataSet.dataValue) * $scope.dollar;
            $scope.Variables.returnFare.dataSet.dataValue = ($scope.Variables.returnFare.dataSet.dataValue) * $scope.dollar;
        } else {
            $scope.Variables.towardsFare.dataSet.dataValue = ($scope.Variables.towardsFare.dataSet.dataValue) / $scope.dollar;
            $scope.Variables.returnFare.dataSet.dataValue = ($scope.Variables.returnFare.dataSet.dataValue) / $scope.dollar;
        }
    };

    $scope.return_flight_livelistClick = function($event, $isolateScope) {
        $scope.calculateAirFare();
    };


    $scope.to_flight_livelistClick = function($event, $isolateScope) {
        $scope.calculateAirFare();
    };


    $scope.selected_tripClick = function($event, $isolateScope) {
        $scope.calculateAirFare();
    };


    $scope.adult_count_textboxClick = function($event, $isolateScope) {
        $scope.calculateAirFare();
    };


    $scope.child_count_textboxClick = function($event, $isolateScope) {
        $scope.calculateAirFare();
    };


    $scope.infant__count_textboxClick = function($event, $isolateScope) {
        $scope.calculateAirFare();
    };




}]);
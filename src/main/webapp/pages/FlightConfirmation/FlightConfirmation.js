Application.$controller("FlightConfirmationPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        debugger
        // [VIBHU]: use svBookingId as pageParam
        if ($scope.Variables.svBookingID.dataSet.dataValue == 0)
            $scope.Variables.goToPage_Main.navigate();
        //$scope.Widgets.sendMailButton.click();
    };

}]);
var _WM_APP_PROPERTIES = {
  "activeTheme" : "turquoise",
  "defaultLanguage" : "en",
  "displayName" : "WaveFlights",
  "homePage" : "Main",
  "name" : "WaveFlights",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};
/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.fdcallcenter.fdcallcenterdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.fdcallcenter.fdcallcenterdb.Airport;
import com.fdcallcenter.fdcallcenterdb.Flight;


/**
 * ServiceImpl object for domain model class Airport.
 *
 * @see Airport
 */
@Service("FDCallCenterDB.AirportService")
public class AirportServiceImpl implements AirportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AirportServiceImpl.class);

    @Autowired
	@Qualifier("FDCallCenterDB.FlightService")
	private FlightService flightService;

    @Autowired
    @Qualifier("FDCallCenterDB.AirportDao")
    private WMGenericDao<Airport, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Airport, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "FDCallCenterDBTransactionManager")
    @Override
	public Airport create(Airport airport) {
        LOGGER.debug("Creating a new Airport with information: {}", airport);
        Airport airportCreated = this.wmGenericDao.create(airport);
        if(airportCreated.getFlightsForDestinationAirport() != null) {
            for(Flight flightsForDestinationAirport : airportCreated.getFlightsForDestinationAirport()) {
                flightsForDestinationAirport.setAirportByDestinationAirport(airportCreated);
                LOGGER.debug("Creating a new child Flight with information: {}", flightsForDestinationAirport);
                flightService.create(flightsForDestinationAirport);
            }
        }

        if(airportCreated.getFlightsForSourceAirport() != null) {
            for(Flight flightsForSourceAirport : airportCreated.getFlightsForSourceAirport()) {
                flightsForSourceAirport.setAirportBySourceAirport(airportCreated);
                LOGGER.debug("Creating a new child Flight with information: {}", flightsForSourceAirport);
                flightService.create(flightsForSourceAirport);
            }
        }
        return airportCreated;
    }

	@Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public Airport getById(String airportId) throws EntityNotFoundException {
        LOGGER.debug("Finding Airport by id: {}", airportId);
        Airport airport = this.wmGenericDao.findById(airportId);
        if (airport == null){
            LOGGER.debug("No Airport found with id: {}", airportId);
            throw new EntityNotFoundException(String.valueOf(airportId));
        }
        return airport;
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public Airport findById(String airportId) {
        LOGGER.debug("Finding Airport by id: {}", airportId);
        return this.wmGenericDao.findById(airportId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "FDCallCenterDBTransactionManager")
	@Override
	public Airport update(Airport airport) throws EntityNotFoundException {
        LOGGER.debug("Updating Airport with information: {}", airport);
        this.wmGenericDao.update(airport);

        String airportId = airport.getCode();

        return this.wmGenericDao.findById(airportId);
    }

    @Transactional(value = "FDCallCenterDBTransactionManager")
	@Override
	public Airport delete(String airportId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Airport with id: {}", airportId);
        Airport deleted = this.wmGenericDao.findById(airportId);
        if (deleted == null) {
            LOGGER.debug("No Airport found with id: {}", airportId);
            throw new EntityNotFoundException(String.valueOf(airportId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public Page<Airport> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Airports");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
    @Override
    public Page<Airport> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Airports");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service FDCallCenterDB for table Airport to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
    @Override
    public Page<Flight> findAssociatedFlightsForDestinationAirport(String code, Pageable pageable) {
        LOGGER.debug("Fetching all associated flightsForDestinationAirport");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("airportByDestinationAirport.code = '" + code + "'");

        return flightService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
    @Override
    public Page<Flight> findAssociatedFlightsForSourceAirport(String code, Pageable pageable) {
        LOGGER.debug("Fetching all associated flightsForSourceAirport");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("airportBySourceAirport.code = '" + code + "'");

        return flightService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service FlightService instance
	 */
	protected void setFlightService(FlightService service) {
        this.flightService = service;
    }

}


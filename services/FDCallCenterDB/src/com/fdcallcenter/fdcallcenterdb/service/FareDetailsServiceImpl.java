/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.fdcallcenter.fdcallcenterdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.fdcallcenter.fdcallcenterdb.FareDetails;
import com.fdcallcenter.fdcallcenterdb.Flight;


/**
 * ServiceImpl object for domain model class FareDetails.
 *
 * @see FareDetails
 */
@Service("FDCallCenterDB.FareDetailsService")
public class FareDetailsServiceImpl implements FareDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FareDetailsServiceImpl.class);

    @Autowired
	@Qualifier("FDCallCenterDB.FlightService")
	private FlightService flightService;

    @Autowired
    @Qualifier("FDCallCenterDB.FareDetailsDao")
    private WMGenericDao<FareDetails, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<FareDetails, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "FDCallCenterDBTransactionManager")
    @Override
	public FareDetails create(FareDetails fareDetails) {
        LOGGER.debug("Creating a new FareDetails with information: {}", fareDetails);
        FareDetails fareDetailsCreated = this.wmGenericDao.create(fareDetails);
        if(fareDetailsCreated.getFlights() != null) {
            for(Flight flight : fareDetailsCreated.getFlights()) {
                flight.setFareDetailsByFareDetails(fareDetailsCreated);
                LOGGER.debug("Creating a new child Flight with information: {}", flight);
                flightService.create(flight);
            }
        }
        return fareDetailsCreated;
    }

	@Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public FareDetails getById(Integer faredetailsId) throws EntityNotFoundException {
        LOGGER.debug("Finding FareDetails by id: {}", faredetailsId);
        FareDetails fareDetails = this.wmGenericDao.findById(faredetailsId);
        if (fareDetails == null){
            LOGGER.debug("No FareDetails found with id: {}", faredetailsId);
            throw new EntityNotFoundException(String.valueOf(faredetailsId));
        }
        return fareDetails;
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public FareDetails findById(Integer faredetailsId) {
        LOGGER.debug("Finding FareDetails by id: {}", faredetailsId);
        return this.wmGenericDao.findById(faredetailsId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "FDCallCenterDBTransactionManager")
	@Override
	public FareDetails update(FareDetails fareDetails) throws EntityNotFoundException {
        LOGGER.debug("Updating FareDetails with information: {}", fareDetails);
        this.wmGenericDao.update(fareDetails);

        Integer faredetailsId = fareDetails.getId();

        return this.wmGenericDao.findById(faredetailsId);
    }

    @Transactional(value = "FDCallCenterDBTransactionManager")
	@Override
	public FareDetails delete(Integer faredetailsId) throws EntityNotFoundException {
        LOGGER.debug("Deleting FareDetails with id: {}", faredetailsId);
        FareDetails deleted = this.wmGenericDao.findById(faredetailsId);
        if (deleted == null) {
            LOGGER.debug("No FareDetails found with id: {}", faredetailsId);
            throw new EntityNotFoundException(String.valueOf(faredetailsId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public Page<FareDetails> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all FareDetails");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
    @Override
    public Page<FareDetails> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all FareDetails");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service FDCallCenterDB for table FareDetails to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "FDCallCenterDBTransactionManager")
    @Override
    public Page<Flight> findAssociatedFlights(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated flights");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("fareDetailsByFareDetails.id = '" + id + "'");

        return flightService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service FlightService instance
	 */
	protected void setFlightService(FlightService service) {
        this.flightService = service;
    }

}


/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.fdcallcenter.fdcallcenterdb;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * FareDetails generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`FARE_DETAILS`")
public class FareDetails implements Serializable {

    private Integer id;
    private Integer economyKtpcChild;
    private Integer economyKtpcAdult;
    private Integer economyQtpcChild;
    private Integer economyQtpcAdult;
    private Integer fuel;
    private List<Flight> flights = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`ECONOMY_KTPC_CHILD`", nullable = true, scale = 0, precision = 10)
    public Integer getEconomyKtpcChild() {
        return this.economyKtpcChild;
    }

    public void setEconomyKtpcChild(Integer economyKtpcChild) {
        this.economyKtpcChild = economyKtpcChild;
    }

    @Column(name = "`ECONOMY_KTPC_ADULT`", nullable = true, scale = 0, precision = 10)
    public Integer getEconomyKtpcAdult() {
        return this.economyKtpcAdult;
    }

    public void setEconomyKtpcAdult(Integer economyKtpcAdult) {
        this.economyKtpcAdult = economyKtpcAdult;
    }

    @Column(name = "`ECONOMY_QTPC_CHILD`", nullable = true, scale = 0, precision = 10)
    public Integer getEconomyQtpcChild() {
        return this.economyQtpcChild;
    }

    public void setEconomyQtpcChild(Integer economyQtpcChild) {
        this.economyQtpcChild = economyQtpcChild;
    }

    @Column(name = "`ECONOMY_QTPC_ADULT`", nullable = true, scale = 0, precision = 10)
    public Integer getEconomyQtpcAdult() {
        return this.economyQtpcAdult;
    }

    public void setEconomyQtpcAdult(Integer economyQtpcAdult) {
        this.economyQtpcAdult = economyQtpcAdult;
    }

    @Column(name = "`FUEL`", nullable = true, scale = 0, precision = 10)
    public Integer getFuel() {
        return this.fuel;
    }

    public void setFuel(Integer fuel) {
        this.fuel = fuel;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "fareDetailsByFareDetails")
    public List<Flight> getFlights() {
        return this.flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FareDetails)) return false;
        final FareDetails fareDetails = (FareDetails) o;
        return Objects.equals(getId(), fareDetails.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}


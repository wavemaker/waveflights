/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.fdcallcenter.fdcallcenterdb.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.fdcallcenter.fdcallcenterdb.Booking;
import com.fdcallcenter.fdcallcenterdb.Passengergroup;
import com.fdcallcenter.fdcallcenterdb.service.BookingService;

/**
 * Controller object for domain model class Booking.
 * @see Booking
 */
@RestController("FDCallCenterDB.BookingController")
@Api(value = "BookingController", description = "Exposes APIs to work with Booking resource.")
@RequestMapping("/FDCallCenterDB/Booking")
public class BookingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingController.class);

    @Autowired
    @Qualifier("FDCallCenterDB.BookingService")
    private BookingService bookingService;

    @ApiOperation(value = "Creates a new Booking instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Booking createBooking(@RequestBody Booking booking) {
        LOGGER.debug("Create Booking with information: {}", booking);
        booking = bookingService.create(booking);
        LOGGER.debug("Created Booking with information: {}", booking);
        return booking;
    }

    @ApiOperation(value = "Returns the Booking instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Booking getBooking(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Booking with id: {}", id);
        Booking foundBooking = bookingService.getById(id);
        LOGGER.debug("Booking details with id: {}", foundBooking);
        return foundBooking;
    }

    @ApiOperation(value = "Updates the Booking instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Booking editBooking(@PathVariable("id") Integer id, @RequestBody Booking booking) throws EntityNotFoundException {
        LOGGER.debug("Editing Booking with id: {}", booking.getId());
        booking.setId(id);
        booking = bookingService.update(booking);
        LOGGER.debug("Booking details with id: {}", booking);
        return booking;
    }

    @ApiOperation(value = "Deletes the Booking instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteBooking(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Booking with id: {}", id);
        Booking deletedBooking = bookingService.delete(id);
        return deletedBooking != null;
    }

    /**
     * @deprecated Use {@link #findBookings(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Booking instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Booking> searchBookingsByQueryFilters(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Bookings list");
        return bookingService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the list of Booking instances matching the search criteria.")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Booking> findBookings(@ApiParam("") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Bookings list");
        return bookingService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data.")
    @RequestMapping(value = "/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportBookings(@PathVariable("exportType") ExportType exportType, @ApiParam("") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        return bookingService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns the total count of Booking instances.")
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Long countBookings(@ApiParam("") @RequestParam(value = "q", required = false) String query) {
        LOGGER.debug("counting Bookings");
        return bookingService.count(query);
    }

    @RequestMapping(value = "/{id}/passengergroups", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the passengergroups instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Passengergroup> findAssociatedPassengergroups(@PathVariable("id") Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated passengergroups");
        return bookingService.findAssociatedPassengergroups(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service BookingService instance
	 */
    protected void setBookingService(BookingService service) {
        this.bookingService = service;
    }
}
